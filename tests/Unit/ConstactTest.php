<?php

namespace Tests\Unit;

use App\Models\Contacts\Contact;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ConstactTest extends \Tests\TestCase
{
    use RefreshDatabase;

    /**
     * @var Contact $contact
     */
    protected Contact $contact;

    /**
     * @var array $data
     */
    protected array $data;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->contact = Contact::create(
            [
                'name' => 'Имя',
                'phone' => '123123',
                'email' => 'asdasd@email.com',
                'position' => 'Должность'
            ]
        );

        $this->data = [
            'name' => 'Новое имя',
            'phone' => '321321',
            'email' => 'asdasd@email.asd',
            'position' => 'Новая должность'
        ];
    }

    /**
     * @return void
     * @throws \Throwable
     */
    public function testItCreatesContact()
    {
        $resp = $this->post('/api/contacts', $this->data);
        $resp->assertOk();
        $content = $resp->decodeResponseJson();
        collect($this->data)->map(fn($v, $k) => $this->assertEquals($v, $content['data'][$k]));
    }

    /**
     * @return void
     * @throws \Throwable
     */
    public function testItUpdatesContact()
    {
        $resp = $this->patch('/api/contacts/'.$this->contact->id, $this->data);
        $resp->assertOk();
        $content = $resp->decodeResponseJson();
        collect($this->data)->map(fn($v, $k) => $this->assertEquals($v, $content['data'][$k]));
    }

    /**
     * @return void
     * @throws \Throwable
     */
    public function testItDeletesContact()
    {
        $resp = $this->delete('/api/contacts/'.$this->contact->id, $this->data);
        $resp->assertOk();
    }
}
