<?php

namespace Tests;

use App\Models\User;

trait ActsWith
{
    /**
     * Undocumented function
     *
     * @param  User|null $user
     * @return $this
     */
    public function actsWithToken(User $user=null)
    {
        $user  = $user ?? $this->user;
        $token = auth('api')->login($user);
        return $this->actingAs($user)
            ->withHeader('Authorization', "Bearer $token");
    }
}
