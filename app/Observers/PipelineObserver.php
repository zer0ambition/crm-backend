<?php

namespace App\Observers;

use App\Models\Pipelines\Pipeline;

class PipelineObserver
{
    /**
     * Handle the Pipeline "created" event.
     *
     * @param  Pipeline $pipeline
     * @return void
     */
    public function created(Pipeline $pipeline)
    {
        $pipeline->statuses()->createMany(
            [
                [
                    'color' => '#ff9800',
                    'name' => 'Первичный контакт'
                ],
                [
                    'color' => '#00b0ff',
                    'name' => 'Переговоры'
                ],
                [
                    'color' => '#ef5350',
                    'name' => 'Принимают решение'
                ],
            ]
        );
    }
}
