<?php


namespace App\Http\Filters;


use Illuminate\Support\Str;

class PipelineFilter extends BaseFilter
{
    public const FIELDS_TO_FILTRATE = [
        'id',
        'name',
        'createdAt'
    ];

    /**
     * Search by name
     *
     * @param  $value
     * @return void
     */
    public function name($value)
    {
        $this->builder->where('name', 'like', "%$value%");
    }

    /**
     * Sort by param
     *
     * @param  $value
     * @return void
     */
    public function sortBy($value)
    {
        if (in_array($value, self::FIELDS_TO_FILTRATE)) {
            $this->builder->orderBy(Str::snake($value), $this->sortDirection);
        }

        if ($value === 'numberOfLeads') {
            $this->builder->withCount('leads')->orderBy('leads_count', $this->sortDirection);
        }
    }
}
