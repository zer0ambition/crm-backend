<?php


namespace App\Http\Filters;


use Illuminate\Support\Str;

class LeadFilter extends BaseFilter
{
    public const FIELDS_TO_FILTRATE = [
        'id',
        'name',
        'vendorCode',
        'price',
        'createdAt',
        'statusId',
        'contactId'
    ];

    /**
     * Search by name
     *
     * @param  $value
     * @return void
     */
    public function name($value)
    {
        $this->builder->where('name', 'like', "%$value%");
    }

    /**
     * Search by pipeline ids
     *
     * @param  $value
     * @return void
     */
    public function pipelines($value)
    {
        $this->builder->whereIn('pipeline_id', $value);
    }

    /**
     * Search by status ids
     *
     * @param  $value
     * @return void
     */
    public function statuses($value)
    {
        $this->builder->whereIn('status_id', $value);
    }

    /**
     * Search by contact ids
     *
     * @param  $value
     * @return void
     */
    public function contacts($value)
    {
        $this->builder->whereIn('contact_id', $value);
    }

    /**
     * Sort by param
     *
     * @param  $value
     * @return void
     */
    public function sortBy($value)
    {
        if (in_array($value, self::FIELDS_TO_FILTRATE)) {
            $this->builder->orderBy(Str::snake($value), $this->sortDirection);
        }
    }
}
