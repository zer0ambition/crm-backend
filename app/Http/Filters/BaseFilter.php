<?php


namespace App\Http\Filters;


use App\Constants\RequestConstants;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

abstract class BaseFilter
{

    /**
     * @var string
     */
    public string $sortDirection;

    /**
     * @var Builder $builder
     */
    protected Builder $builder;

    /**
     * @var Request
     */
    protected Request $request;

    /**
     * BaseFilter constructor.
     *
     * @param mixed   $builder
     * @param Request $request
     */
    public function __construct($builder, Request $request)
    {
        $this->builder       = $builder;
        $this->request       = $request;
        $this->sortDirection = $request->sortDirection ?? RequestConstants::DEFAULT_SORT_DIRECTION;
    }

    /**
     * Apply filters
     *
     * @return mixed
     */
    public function apply()
    {
        foreach ($this->filters() as $filter => $value) {
            if (method_exists($this, $filter)) {
                $this->$filter($value);
            }
        }

        return $this->builder;
    }

    /**
     * Get filters from request
     *
     * @return array
     */
    public function filters(): array
    {
        return $this->request->all();
    }
}
