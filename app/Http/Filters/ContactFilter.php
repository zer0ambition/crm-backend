<?php


namespace App\Http\Filters;


use Illuminate\Support\Str;

class ContactFilter extends BaseFilter
{
    public const FIELDS_TO_FILTRATE = [
        'id',
        'name',
        'phone',
        'email',
        'createdAt'
    ];

    /**
     * Search by name
     *
     * @param  $value
     * @return void
     */
    public function name($value)
    {
        $this->builder->where('name', 'like', "%$value%");
    }

    /**
     * Search by phone
     *
     * @param  $value
     * @return void
     */
    public function phone($value)
    {
        $this->builder->where('phone', 'like', "%$value%");
    }

    /**
     * Search by email
     *
     * @param  $value
     * @return void
     */
    public function email($value)
    {
        $this->builder->where('email', 'like', "%$value%");
    }

    /**
     * Sort by param
     *
     * @param  $value
     * @return void
     */
    public function sortBy($value)
    {
        if (in_array($value, self::FIELDS_TO_FILTRATE)) {
            $this->builder->orderBy(Str::snake($value), $this->sortDirection);
        }
    }
}
