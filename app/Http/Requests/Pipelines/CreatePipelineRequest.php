<?php

namespace App\Http\Requests\Pipelines;

use App\Traits\Requests\CaseChangeable;
use App\Traits\Requests\Throwable;
use Illuminate\Foundation\Http\FormRequest;

class CreatePipelineRequest extends FormRequest
{
    use Throwable, CaseChangeable;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'name' => 'required|string',
            'description' => 'sometimes|string'
        ];
    }
}
