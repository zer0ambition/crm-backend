<?php

namespace App\Http\Requests\Pipelines;

use App\Http\Resources\Pipelines\PipelineResource;
use App\Traits\Requests\Paginable;
use App\Traits\Requests\Throwable;
use Illuminate\Foundation\Http\FormRequest;

class ListPipelinesRequest extends FormRequest
{
    use Throwable, Paginable;

    /**
     * @var PipelineResource $resource
     */
    protected PipelineResource $resource;

    /**
     * ListUsersRequest constructor.
     *
     * @param array $query
     * @param array $request
     * @param array $attributes
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param null  $content
     */
    public function __construct(array $query=[], array $request=[], array $attributes=[], array $cookies=[], array $files=[], array $server=[], $content=null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->resource = new PipelineResource([]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'name' => 'string'
        ] + $this->getPaginationRules($this->resource->getAttributes());
    }
}
