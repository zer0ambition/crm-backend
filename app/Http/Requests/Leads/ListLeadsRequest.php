<?php

namespace App\Http\Requests\Leads;

use App\Http\Resources\Leads\LeadResource;
use App\Traits\Requests\Paginable;
use App\Traits\Requests\Throwable;
use Illuminate\Foundation\Http\FormRequest;

class ListLeadsRequest extends FormRequest
{
    use Throwable, Paginable;

    /**
     * @var LeadResource $resource
     */
    protected LeadResource $resource;

    /**
     * ListUsersRequest constructor.
     *
     * @param array $query
     * @param array $request
     * @param array $attributes
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param null  $content
     */
    public function __construct(array $query=[], array $request=[], array $attributes=[], array $cookies=[], array $files=[], array $server=[], $content=null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->resource = new LeadResource([]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'name' => 'string',
            'statuses' => 'array',
            'statuses.*' => 'numeric',
            'pipelines' => 'array',
            'pipelines.*' => 'numeric',
            'contacts' => 'array',
            'contacts.*' => 'numeric'
        ] + $this->getPaginationRules($this->resource->getAttributes());
    }
}
