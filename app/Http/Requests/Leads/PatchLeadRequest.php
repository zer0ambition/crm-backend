<?php

namespace App\Http\Requests\Leads;

use App\Traits\Requests\CaseChangeable;
use App\Traits\Requests\Throwable;
use Illuminate\Foundation\Http\FormRequest;

class PatchLeadRequest extends FormRequest
{
    use Throwable, CaseChangeable;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'name' => 'sometimes|string',
            'vendorCode' => 'sometimes|string',
            'price' => 'sometimes|numeric',
            'statusId' => 'sometimes|exists:lead_statuses,id',
            'pipelineId' => 'sometimes|exists:pipelines,id',
            'contactId' => 'sometimes|exists:contacts,id'
        ];
    }
}
