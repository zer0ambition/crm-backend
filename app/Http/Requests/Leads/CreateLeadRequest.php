<?php

namespace App\Http\Requests\Leads;

use App\Traits\Requests\CaseChangeable;
use App\Traits\Requests\Throwable;
use Illuminate\Foundation\Http\FormRequest;

class CreateLeadRequest extends FormRequest
{
    use Throwable, CaseChangeable;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'name' => 'required|string',
            'vendorCode' => 'required|string',
            'price' => 'required|numeric',
            'statusId' => 'required|exists:lead_statuses,id',
            'pipelineId' => 'required|exists:pipelines,id',
            'contactId' => 'required|exists:contacts,id'
        ];
    }
}
