<?php

namespace App\Http\Requests\Contacts;

use App\Models\Contacts\Contact;
use App\Traits\Requests\CaseChangeable;
use App\Traits\Requests\Throwable;
use Illuminate\Foundation\Http\FormRequest;

class PatchContactRequest extends FormRequest
{
    use Throwable, CaseChangeable;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        $resolver = $this->getRouteResolver()();
        $id       = null;
        foreach ($resolver->parameters as $param) {
            if ($param instanceof Contact) {
                $id = $param->id ?? '';
            }
        }

        return [
            'name' => 'sometimes|string',
            'phone' => 'sometimes|unique:contacts,phone,'.$id,
            'email' => 'sometimes|unique:contacts,email,'.$id.'|email',
            'position' => 'sometimes|string'
        ];
    }
}
