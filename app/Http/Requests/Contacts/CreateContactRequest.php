<?php

namespace App\Http\Requests\Contacts;

use App\Traits\Requests\CaseChangeable;
use App\Traits\Requests\Throwable;
use Illuminate\Foundation\Http\FormRequest;

class CreateContactRequest extends FormRequest
{
    use Throwable, CaseChangeable;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'name' => 'required|string',
            'phone' => 'required|unique:contacts,phone',
            'email' => 'required|unique:contacts,email|email',
            'position' => 'sometimes|string|nullable'
        ];
    }
}
