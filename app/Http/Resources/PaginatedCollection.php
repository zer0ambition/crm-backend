<?php

namespace App\Http\Resources;

use App\Interfaces\Resources\IResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PaginatedCollection extends ResourceCollection
{

    /**
     * @var string $resourceClass
     */
    private string $resourceClass;

    /**
     * @var array $fields
     */
    private array $fields;

    /**
     * PaginatedCollection constructor.
     *
     * @param $resource
     * @param $resourceClass
     */
    public function __construct($resource, $resourceClass)
    {
        parent::__construct($resource);
        $this->resourceClass = $resourceClass;
    }

    /**
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request) : array
    {
        /**
         * @var IResource
         */
        $resourceCollection = (new $this->resourceClass($this->collection));
        $fields      = $request->input('fields', []);
        $query       = $request->query;
        $nextPageUrl = '';
        $prevPageUrl = '';
        if ($this->resource->currentPage() < $this->resource->lastPage()) {
            $query->set('page', intval($query->get('page')) + 1);
            $nextPageUrl = url()->current().'?'.http_build_query($request->query->all());
        }

        if ($this->resource->currentPage() > 1) {
            $query->set('page', intval($query->get('page')) - 1);
            $nextPageUrl = url()->current().'?'.http_build_query($request->query->all());
        }

        return [
            'items' => $resourceCollection->only(!empty($fields) ? $fields : $resourceCollection->getAttributes()),
            'total' => $this->resource->total(),
            'page' => $this->resource->currentPage(),
            'limit' => $this->resource->perPage(),
            'lastPage' => $this->resource->lastPage(),
            'from' => $this->resource->firstItem(),
            'to' => $this->resource->lastItem(),
            'nextPageUrl' => $nextPageUrl,
            'prevPageUrl' => $prevPageUrl,
            'url' => $this->resource->url(url()->current().'?'.http_build_query($request->query))
        ];
    }
}
