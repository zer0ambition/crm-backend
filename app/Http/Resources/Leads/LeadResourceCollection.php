<?php

namespace App\Http\Resources\Leads;

use App\Http\Resources\Contacts\ContactResource;
use App\Http\Resources\LeadStatuses\LeadStatusResource;
use App\Http\Resources\Pipelines\PipelineResource;
use App\Interfaces\Resources\IResource;
use App\Models\Leads\Lead;
use App\Traits\Resources\Filterable;
use App\Traits\Resources\HasAttributes;
use Illuminate\Http\Resources\Json\ResourceCollection;

class LeadResourceCollection extends ResourceCollection implements IResource
{
    use Filterable, HasAttributes;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request) : array
    {
        return $this->processCollection($request);
    }

    /**
     * Get resource fields with values
     *
     * @return array
     */
    public function getFields() : array
    {
        $contact  = null;
        $pipeline = null;
        $status   = null;
        if ($this->resource instanceof Lead) {
            $contact  = $this->contact ? new ContactResource($this->contact) : $contact;
            $status   = $this->status ? new LeadStatusResource($this->status) : $status;
            $pipeline = $this->pipeline ? new PipelineResource($this->pipeline) : $pipeline;
        }

        return [
            'id' => $this->id ?? '',
            'name' => $this->name ?? '',
            'createdAt' => $this->created_at ?? '',
            'updatedAt' => $this->updated_at ?? '',
            'price' => $this->price ?? '',
            'vendorCode' => $this->vendor_code ?? '',
            'status' => $status,
            'pipeline' => $pipeline,
            'contact' => $contact
        ];
    }
}
