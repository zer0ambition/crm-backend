<?php

namespace App\Http\Resources\Contacts;

use App\Interfaces\Resources\IResource;
use App\Models\Contacts\Contact;
use App\Traits\Resources\Filterable;
use App\Traits\Resources\HasAttributes;
use Illuminate\Http\Resources\Json\JsonResource;

class ContactResource extends JsonResource implements IResource
{
    use Filterable, HasAttributes;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request) : array
    {
        return $this->filterFields($this->getFields());
    }

    /**
     * Get resource fields with values
     *
     * @return array
     */
    public function getFields() : array
    {
        $numberOfLeads = 0;
        if ($this->resource instanceof Contact) {
            $numberOfLeads = $this->leads()->count();
        }

        return [
            'id' => $this->id ?? '',
            'name' => $this->name ?? '',
            'createdAt' => $this->created_at ?? '',
            'updatedAt' => $this->updated_at ?? '',
            'phone' => $this->phone ?? '',
            'email' => $this->email ?? '',
            'position' => $this->position ?? '',
            'numberOfLeads' => $numberOfLeads
        ];
    }
}
