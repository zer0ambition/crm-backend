<?php

namespace App\Http\Resources\Pipelines;

use App\Http\Resources\LeadStatuses\LeadStatusResource;
use App\Interfaces\Resources\IResource;
use App\Models\Pipelines\Pipeline;
use App\Traits\Resources\Filterable;
use App\Traits\Resources\HasAttributes;
use Illuminate\Http\Resources\Json\JsonResource;

class PipelineResource extends JsonResource implements IResource
{
    use Filterable, HasAttributes;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request) : array
    {
        return $this->filterFields($this->getFields());
    }

    /**
     * Get resource fields with values
     *
     * @return array
     */
    public function getFields() : array
    {
        $numberOfLeads = 0;
        $statuses      = null;
        if ($this->resource instanceof Pipeline) {
            $numberOfLeads = $this->leads()->count();
            $statuses      = $this->statuses ? LeadStatusResource::collection($this->statuses) : $statuses;
        }

        return [
            'id' => $this->id ?? '',
            'name' => $this->name ?? '',
            'description' => $this->description ?? '',
            'createdAt' => $this->created_at ?? '',
            'updatedAt' => $this->updated_at ?? '',
            'numberOfLeads' => $numberOfLeads,
            'statuses' => $statuses
        ];
    }
}
