<?php

namespace App\Http\Resources\LeadStatuses;

use Illuminate\Http\Resources\Json\JsonResource;

class LeadStatusResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request) : array
    {
        return [
            'id' => $this->id ?? '',
            'name' => $this->name ?? '',
            'color' => $this->color ?? ''
        ];
    }
}
