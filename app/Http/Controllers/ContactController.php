<?php

namespace App\Http\Controllers;

use App\Constants\RequestConstants;
use App\Http\Filters\ContactFilter;
use App\Http\Requests\Contacts\CreateContactRequest;
use App\Http\Requests\Contacts\ListContactsRequest;
use App\Http\Requests\Contacts\PatchContactRequest;
use App\Http\Resources\Contacts\ContactResource;
use App\Http\Resources\Contacts\ContactResourceCollection;
use App\Http\Resources\PaginatedCollection;
use App\Http\Response\ErrorResponse;
use App\Http\Response\JsonResponse;
use App\Models\Contacts\Contact;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  ListContactsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ListContactsRequest $request)
    {
        $contacts = (new ContactFilter(Contact::query(), $request))
            ->apply()->paginate(intval($request->input('limit')) ?: RequestConstants::DEFAULT_LIMIT);
        return new JsonResponse(
            [
                'data' => new PaginatedCollection(
                    $contacts,
                    ContactResourceCollection::class
                ),
                'message' => trans('http.success')
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateContactRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function create(CreateContactRequest $request)
    {
        $data = $request->validatedToSnakeCase();
        DB::beginTransaction();
        try {
            $contact = Contact::create($data);
        } catch (\Exception $e) {
            DB::rollBack();
            return new ErrorResponse(['errors' => $e->getMessage(), 'message' => trans('http.error')], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        DB::commit();

        return new JsonResponse(['data' => new ContactResource($contact), 'message' => trans('http.create', ['item' => trans('models.contact')])]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Contact $contact
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Contact $contact)
    {
        return new JsonResponse(['data' => new ContactResource($contact), 'message' => trans('http.success')]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PatchContactRequest $request
     * @param  Contact             $contact
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PatchContactRequest $request, Contact $contact)
    {
        $data = $request->validatedToSnakeCase();
        DB::beginTransaction();
        try {
            $contact->update($data);
        } catch (\Exception $e) {
            DB::rollBack();
            return new ErrorResponse(['errors' => $e->getMessage(), 'message' => trans('http.error')], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        DB::commit();

        return new JsonResponse(['data' => new ContactResource($contact), 'message' => trans('http.patch', ['item' => trans('models.contacts')])]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Contact $contact
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Contact $contact)
    {
        DB::beginTransaction();
        try {
            $contact->delete();
        } catch (\Exception $e) {
            DB::rollBack();
            return new ErrorResponse($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        DB::commit();

        return new JsonResponse(['message' => trans('http.delete', ['item' => trans('models.contact')])]);
    }
}
