<?php

namespace App\Http\Controllers;

use App\Constants\RequestConstants;
use App\Http\Filters\PipelineFilter;
use App\Http\Requests\Pipelines\CreatePipelineRequest;
use App\Http\Requests\Pipelines\ListPipelinesRequest;
use App\Http\Requests\Pipelines\PatchPipelineRequest;
use App\Http\Resources\PaginatedCollection;
use App\Http\Resources\Pipelines\PipelineResource;
use App\Http\Resources\Pipelines\PipelineResourceCollection;
use App\Http\Response\ErrorResponse;
use App\Http\Response\JsonResponse;
use App\Models\Pipelines\Pipeline;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class PipelineController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  ListPipelinesRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ListPipelinesRequest $request)
    {
        $pipelines = (new PipelineFilter(Pipeline::query(), $request))
            ->apply()->paginate(intval($request->input('limit')) ?: RequestConstants::DEFAULT_LIMIT);
        return new JsonResponse(
            [
                'data' => new PaginatedCollection(
                    $pipelines,
                    PipelineResourceCollection::class
                ),
                'message' => trans('http.success')
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  CreatePipelineRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function create(CreatePipelineRequest $request)
    {
        $data = $request->validatedToSnakeCase();
        DB::beginTransaction();
        try {
            $pipeline = Pipeline::create($data);
        } catch (\Exception $e) {
            DB::rollBack();
            return new ErrorResponse(['errors' => $e->getMessage(), 'message' => trans('http.error')], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        DB::commit();

        return new JsonResponse(['data' => new PipelineResource($pipeline), 'message' => trans('http.create', ['item' => trans('models.pipeline')])]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Pipeline $pipeline
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Pipeline $pipeline)
    {
        return new JsonResponse(['data' => new PipelineResource($pipeline), 'message' => trans('http.success')]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PatchPipelineRequest $request
     * @param  Pipeline             $pipeline
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function update(PatchPipelineRequest $request, Pipeline $pipeline)
    {
        $data = $request->validatedToSnakeCase();
        DB::beginTransaction();
        try {
            $pipeline->update($data);
            if (isset($data['statuses'])) {
                $statuses = collect($data['statuses'])->map(fn($s) => $pipeline->statuses()->updateOrCreate(['id' => $s['id'] ?? 0], $s));
                $pipeline->statuses()->whereNotIn('id', $statuses->pluck('id'))->delete();
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return new ErrorResponse(['errors' => $e->getMessage(), 'message' => trans('http.error')], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        DB::commit();

        return new JsonResponse(['data' => new PipelineResource($pipeline), 'message' => trans('http.patch', ['item' => trans('models.pipeline')])]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Pipeline $pipeline
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function destroy(Pipeline $pipeline)
    {
        DB::beginTransaction();
        try {
            $pipeline->statuses()->delete();
            $pipeline->delete();
        } catch (\Exception $e) {
            DB::rollBack();
            return new ErrorResponse($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        DB::commit();

        return new JsonResponse(['message' => trans('http.delete', ['item' => trans('models.pipeline')])]);
    }
}
