<?php

namespace App\Http\Controllers;

use App\Constants\RequestConstants;
use App\Http\Filters\LeadFilter;
use App\Http\Requests\Leads\CreateLeadRequest;
use App\Http\Requests\Leads\ListLeadsRequest;
use App\Http\Requests\Leads\PatchLeadRequest;
use App\Http\Resources\Leads\LeadResource;
use App\Http\Resources\Leads\LeadResourceCollection;
use App\Http\Resources\PaginatedCollection;
use App\Http\Response\ErrorResponse;
use App\Models\Leads\Lead;
use App\Http\Response\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  ListLeadsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ListLeadsRequest $request)
    {
        $pipelines = (new LeadFilter(Lead::query(), $request))
            ->apply()->paginate(intval($request->input('limit')) ?: RequestConstants::DEFAULT_LIMIT);
        return new JsonResponse(
            [
                'data' => new PaginatedCollection(
                    $pipelines,
                    LeadResourceCollection::class
                ),
                'message' => trans('http.success')
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateLeadRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function create(CreateLeadRequest $request)
    {
        $data = $request->validatedToSnakeCase();
        DB::beginTransaction();
        try {
            $lead = Lead::create($data);
        } catch (\Exception $e) {
            DB::rollBack();
            return new ErrorResponse(['errors' => $e->getMessage(), 'message' => trans('http.error')], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        DB::commit();

        return new JsonResponse(['data' => new LeadResource($lead), 'message' => trans('http.create', ['item' => trans('models.lead')])]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Lead $lead
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Lead $lead)
    {
        return new JsonResponse(['data' => new LeadResource($lead), 'message' => trans('http.success')]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PatchLeadRequest $request
     * @param  Lead             $lead
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PatchLeadRequest $request, Lead $lead)
    {
        $data = $request->validatedToSnakeCase();
        DB::beginTransaction();
        try {
            $lead->update($data);
        } catch (\Exception $e) {
            DB::rollBack();
            return new ErrorResponse(['errors' => $e->getMessage(), 'message' => trans('http.error')], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        DB::commit();

        return new JsonResponse(['data' => new LeadResource($lead), 'message' => trans('http.patch', ['item' => trans('models.lead')])]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Lead $lead
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Lead $lead)
    {
        DB::beginTransaction();
        try {
            $lead->delete();
        } catch (\Exception $e) {
            DB::rollBack();
            return new ErrorResponse($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        DB::commit();

        return new JsonResponse(['message' => trans('http.delete', ['item' => trans('models.lead')])]);
    }
}
