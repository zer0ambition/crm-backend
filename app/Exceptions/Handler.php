<?php

namespace App\Exceptions;

use App\Http\Response\ErrorResponse;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(
            function (Throwable $e) {
                //
            }
        );
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Throwable               $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        $msg  = $exception->getMessage();
        $code = Response::HTTP_NOT_FOUND;

        if ($exception instanceof ModelNotFoundException) {
            return new ErrorResponse(['error' => $msg, 'message' => trans('http.resource_not_found', ['resource' => trans('models.'.Str::snake(class_basename($exception->getModel())))])], $code);
        }

        /*
        if ($exception instanceof TokenBlacklistedException) {
            return new ErrorResponse(['error' => $msg, 'message' => trans('http.forbidden')], Response::HTTP_UNAUTHORIZED);
        }*/

        if ($exception instanceof NotFoundHttpException) {
            return new ErrorResponse(['errors' => $exception->getMessage(), 'message' => trans('http.route_not_found')], $code);
        }

        return parent::render($request, $exception);
    }
}
