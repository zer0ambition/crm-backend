<?php

namespace App\Models\Leads;

use App\Models\Contacts\Contact;
use App\Models\LeadStatuses\LeadStatus;
use App\Models\Pipelines\Pipeline;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Lead extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'vendor_code',
        'price',
        'contact_id',
        'pipeline_id',
        'status_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * Relation statuses table
     *
     * @return BelongsTo
     */
    public function status() : BelongsTo
    {
        return $this->belongsTo(LeadStatus::class, 'status_id', 'id');
    }

    /**
     * Relation pipelines table
     *
     * @return BelongsTo
     */
    public function pipeline() : BelongsTo
    {
        return $this->belongsTo(Pipeline::class, 'pipeline_id', 'id');
    }

    /**
     * Relation contacts table
     *
     * @return BelongsTo
     */
    public function contact() : BelongsTo
    {
        return $this->belongsTo(Contact::class, 'contact_id', 'id');
    }
}
