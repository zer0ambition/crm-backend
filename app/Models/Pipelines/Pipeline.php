<?php

namespace App\Models\Pipelines;

use App\Models\Leads\Lead;
use App\Models\LeadStatuses\LeadStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Pipeline extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    /**
     * Relation statuses table
     *
     * @return HasMany
     */
    public function statuses() : HasMany
    {
        return $this->hasMany(LeadStatus::class, 'pipeline_id', 'id');
    }

    /**
     * Relation leads table
     *
     * @return HasMany
     */
    public function leads() : HasMany
    {
        return $this->hasMany(Lead::class, 'pipeline_id', 'id');
    }
}
