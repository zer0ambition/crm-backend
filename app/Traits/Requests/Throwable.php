<?php


namespace App\Traits\Requests;


use App\Http\Response\ErrorResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

trait Throwable
{
    /**
     * Handle a failed validation attempt.
     *
     * @param  Validator $validator
     * @return void
     *
     * @throws HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(new ErrorResponse(['errors' => $validator->errors(), 'message' => trans('http.wrong_data')], Response::HTTP_UNPROCESSABLE_ENTITY));
    }
}
