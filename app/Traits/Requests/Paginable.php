<?php


namespace App\Traits\Requests;


use App\Constants\RequestConstants;
use Illuminate\Validation\Rule;

trait Paginable
{
    /**
     * Get pagination request interface rules
     *
     * @param  array $fields
     * @return array
     */
    public function getPaginationRules(array $fields) : array
    {
        return [
            'fields' => 'array',
            'fields.*' => Rule::in($fields),
            'page' => 'numeric|min:1',
            'limit' => 'numeric',
            'sortBy' => Rule::in($fields),
            'sortDirection' => Rule::in(RequestConstants::SORT_DIRECTIONS)
        ];
    }
}
