<?php


namespace App\Traits\Requests;


use Illuminate\Support\Str;

trait CaseChangeable
{
    /**
     * N-nested array to snake case keys
     *
     * @param  array $array
     * @return array
     */
    public function arrayToSnakeCase(array $array): array
    {
        return array_map(
            function ($item) {
                if (is_array($item)) {
                    $item = $this->arrayToSnakeCase($item);
                    //recursive call
                }

                return $item;
            },
            $this->toSnakeCase($array)
        );
    }

    /**
     * Mutate keys to snake case
     *
     * @param  array $array
     * @return array
     */
    public function toSnakeCase(array $array) : array
    {
        $result = [];
        foreach ($array as $key => $value) {
            $key          = Str::snake($key);
            $result[$key] = $value;
        }

        return $result;
    }

    /**
     * Mutate validated keys to snake case
     *
     * @return array
     */
    public function validatedToSnakeCase() : array
    {
        return $this->arrayToSnakeCase($this->validated());
    }

    /**
     * Mutate all keys to snake case
     *
     * @return array
     */
    public function allToSnakeCase() : array
    {
        return $this->arrayToSnakeCase($this->all());
    }
}
