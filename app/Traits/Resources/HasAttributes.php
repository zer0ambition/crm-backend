<?php


namespace App\Traits\Resources;


trait HasAttributes
{

    /**
     * @return array
     */
    public function getAttributes() : array
    {
        return array_keys($this->getFields());
    }
}
