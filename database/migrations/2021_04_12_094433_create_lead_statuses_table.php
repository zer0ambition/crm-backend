<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('name')->default('');
            $table->string('color')->default('');
            $table->unsignedBigInteger('pipeline_id')->nullable();
            $table->timestamps();
        });

        Schema::table(
            'lead_statuses',
            function (Blueprint $table) {
                $table->foreign('pipeline_id')->references('id')->on('pipelines')->onDelete('SET NULL');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'lead_statuses',
            function (Blueprint $table) {
                if (DB::getDriverName() !== 'sqlite') {
                    $table->dropForeign('lead_statuses_pipeline_id_foreign');
                }
            }
        );

        Schema::dropIfExists('lead_statuses');
    }
}
