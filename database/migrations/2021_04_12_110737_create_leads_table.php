<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->id();
            $table->string('name')->default('');
            $table->string('vendor_code')->default('');
            $table->integer('price')->default(0);
            $table->unsignedBigInteger('status_id')->nullable();
            $table->unsignedBigInteger('pipeline_id')->nullable();
            $table->unsignedBigInteger('contact_id')->nullable();
            $table->timestamps();
        });

        Schema::table(
            'leads',
            function (Blueprint $table) {
                $table->foreign('status_id')->references('id')->on('lead_statuses')->onDelete('SET NULL');
                $table->foreign('pipeline_id')->references('id')->on('pipelines')->onDelete('SET NULL');
                $table->foreign('contact_id')->references('id')->on('contacts')->onDelete('SET NULL');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'leads',
            function (Blueprint $table) {
                if (DB::getDriverName() !== 'sqlite') {
                    $table->dropForeign('leads_status_id_foreign');
                    $table->dropForeign('leads_pipeline_id_foreign');
                    $table->dropForeign('leads_contact_id_foreign');
                }
            }
        );
        Schema::dropIfExists('leads');
    }
}
