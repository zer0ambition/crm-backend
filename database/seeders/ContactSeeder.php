<?php

namespace Database\Seeders;

use App\Models\Contacts\Contact;
use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Contact::where('phone', '89999999999')->exists()) {
            Contact::create([
                'phone' => '89999999999',
                'email' => 'example@mail.ru',
                'name' => 'Иван Иванович',
                'position' => 'Директор'
            ]);
        }

        if (!Contact::where('phone', '81111111111')->exists()) {
            Contact::create([
                'phone' => '81111111111',
                'email' => 'example@mail.com',
                'name' => 'Петр Петрович',
                'position' => 'Директор'
            ]);
        }
    }
}
