<?php

namespace Database\Seeders;

use App\Models\Pipelines\Pipeline;
use Illuminate\Database\Seeder;

class PipelineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Pipeline::where('name', 'Воронка отдела продаж 1')->exists()) {
            Pipeline::create([
                'name' => 'Воронка отдела продаж 1',
                'description' => 'В данной воронке ведут свою деятельность сотрудники отдела продаж 1'
            ]);
        }

        if (!Pipeline::where('name', 'Воронка отдела продаж 2')->exists()) {
            Pipeline::create([
                'name' => 'Воронка отдела продаж 2',
                'description' => 'В данной воронке ведут свою деятельность сотрудники отдела продаж 2'
            ]);
        }

        if (!Pipeline::where('name', 'Воронка отдела продаж 3')->exists()) {
            Pipeline::create([
                'name' => 'Воронка отдела продаж 3',
                'description' => 'В данной воронке ведут свою деятельность сотрудники отдела продаж 3'
            ]);
        }
    }
}
