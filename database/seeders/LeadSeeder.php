<?php

namespace Database\Seeders;

use App\Models\Contacts\Contact;
use App\Models\Leads\Lead;
use App\Models\Pipelines\Pipeline;
use Illuminate\Database\Seeder;

class LeadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contact = Contact::first();
        $pipeline = Pipeline::first();
        $status = $pipeline->statuses()->first();
        if (!Lead::where('name', 'Заявка на покупку товара')->exists()) {
            Lead::create([
                'name' => 'Заявка на покупку товара',
                'vendor_code' => '1233132',
                'price' => 1000,
                'status_id' => $status->id,
                'pipeline_id' => $pipeline->id,
                'contact_id' => $contact->id
            ]);
        }

        if (!Lead::where('name', 'Заявка на оказание услуги')->exists()) {
            Lead::create([
                'name' => 'Заявка на оказание услуги',
                'vendor_code' => '22223322',
                'price' => 3000,
                'status_id' => $status->id,
                'pipeline_id' => $pipeline->id,
                'contact_id' => $contact->id
            ]);
        }
    }
}
