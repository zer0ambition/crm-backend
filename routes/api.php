<?php

use App\Http\Controllers\ContactController;
use App\Http\Controllers\LeadController;
use App\Http\Controllers\PipelineController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//pipelines
Route::prefix('pipelines')->group(
    function () {
        Route::get('/', [PipelineController::class, 'index']);
        Route::post('/', [PipelineController::class, 'create']);

        Route::prefix('{pipeline}')->group(
            function () {
                Route::get('/', [PipelineController::class, 'show']);
                Route::patch('/', [PipelineController::class, 'update']);
                Route::delete('/', [PipelineController::class, 'destroy']);
            }
        );
    }
);

//contacts
Route::prefix('contacts')->group(
    function () {
        Route::get('/', [ContactController::class, 'index']);
        Route::post('/', [ContactController::class, 'create']);

        Route::prefix('{contact}')->group(
            function () {
                Route::get('/', [ContactController::class, 'show']);
                Route::patch('/', [ContactController::class, 'update']);
                Route::delete('/', [ContactController::class, 'destroy']);
            }
        );
    }
);

//leads
Route::prefix('leads')->group(
    function () {
        Route::get('/', [LeadController::class, 'index']);
        Route::post('/', [LeadController::class, 'create']);

        Route::prefix('{lead}')->group(
            function () {
                Route::get('/', [LeadController::class, 'show']);
                Route::patch('/', [LeadController::class, 'update']);
                Route::delete('/', [LeadController::class, 'destroy']);
            }
        );
    }
);
